import pytest
from utils.user.user_type_util import UserType
from users.models import CustomUser, UniversityUser

@pytest.mark.django_db
class TestUserType:
    def setup_method(self):
        self.customUser = CustomUser
        self.universityUser = UniversityUser
        
        self.super_user_type = CustomUser.super_user_type
        self.university_admin_user_type = CustomUser.university_admin_user_type
        self.university_user_type = CustomUser.university_user_type

    def test_invalid_university_user_type(self):
        with pytest.raises(Exception) as e:
            UserType.is_valid_user_type(self.super_user_type, self.universityUser)
        assert str(e.value) == f"Wrong User type ({self.super_user_type}) for this Model User ({UniversityUser})"
    
    def test_valid_university_user_type(self):
        assert UserType.is_valid_user_type(self.university_admin_user_type, self.universityUser) == self.university_admin_user_type

    def test_invalid_custom_user_type(self):
        with pytest.raises(Exception) as e:
            UserType.is_valid_user_type(self.university_user_type, self.customUser)
        assert str(e.value) == f"Wrong User type ({self.university_user_type}) for this Model User ({CustomUser})"
    
    def test_valid_super_user(self):
        assert UserType.is_valid_user_type(self.super_user_type, self.customUser) == self.super_user_type
