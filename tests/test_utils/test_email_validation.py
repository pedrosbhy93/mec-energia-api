import pytest
from utils.email.valid_email import verify_email_is_valid

def test_valid_and_existing_email():
    assert verify_email_is_valid("questes102@gmail.com") == True

def test_incorrect_domain():
    with pytest.raises(Exception):
        verify_email_is_valid("questes102@gmal.com")

def test_invalid_email():
    with pytest.raises(Exception):
        verify_email_is_valid("questes102#gmal.com")

def test_valid_non_existing_email():
    with pytest.raises(Exception):
        verify_email_is_valid("questes10@gmal.com")



