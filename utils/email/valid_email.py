from email_validator import validate_email, EmailNotValidError

def verify_email_is_valid(email):
    try:
        validate_email(email, check_deliverability=True)
    except EmailNotValidError as e:
        raise Exception(str(e))

    return True